#!/bin/bash

coverage run --omit */test_*,*/dtest_*,*__init__.py,/usr/lib/python3/dist-packages/* -m unittest discover -s project -p test*.py -v
coverage report
source runDocker.sh
sleep 2
coverage run --omit */test_*,*/dtest_*,*__init__.py,/usr/lib/python3/dist-packages/* -m unittest discover -s project -p dtest*.py -v
#!/bin/bash
# https://stackoverflow.com/questions/9886268/shell-script-syntax-error-unexpected-end-of-file
CONTAINER_EXISTS=$(docker ps -a | grep "tp2gb" | wc -l)
if (($CONTAINER_EXISTS > 0)); then
	docker stop tp2gb
	docker rm -f tp2gb
fi

IMAGE_EXISTS=$(docker images | grep "tp2gb_image" | wc -l)
if (($IMAGE_EXISTS > 0)); then
	docker image rm -f tp2gb_image
fi

VOLUME_EXISTS=$(docker volume ls | grep "tp2gb_vol" | wc -l)
if (($VOLUME_EXISTS > 0)); then
	docker volume rm -f tp2gb_vol
fi

docker volume create --name tp2gb_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp2gb_image -f ./project/docker/Dockerfile .
docker run -d -p 5555:5555 --mount source=tp2gb_vol,destination=/mnt/app/ --name tp2gb tp2gb_image
docker exec -it tp2gb service rsyslog start
# docker exec -it tp2gb /bin/bash
class InitialUserException(Exception):
	pass

class NonExistingUserException(Exception):
	pass

class UserAlreadyExistsException(Exception):
	pass
#TODO add new custom exception classes here as/if you need them

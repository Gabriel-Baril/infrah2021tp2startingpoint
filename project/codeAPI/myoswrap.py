import subprocess

def runCommandToAddUser(username):
	try:
		subprocess.run(['useradd', username])
	except:
		print("Failed to add user.")

def runCommandToRemoveUser(username):
	try:
		subprocess.run(['userdel', username])
	except:
		print("Failed to delete user.")

def readFileByLine(filename):
	file = open(filename, 'r')
	content = []
	for line in file:
		content.append(line)
	return content
from flask import *
from codeAPI import myoswrap
from codeAPI.customExceptions import *

app = Flask('TP2 API')

def getUserList():
	users = []
	for line in myoswrap.readFileByLine('/etc/passwd'):
		users.append(line.split(':')[0])
	return users

INITIAL_USERS = [
	'root',
	'daemon',
	'bin',
	'sys',
	'sync',
	'games',
	'man',
	'lp',
	'mail',
	'news',
	'uucp',
	'proxy',
	'www-data',
	'backup',
	'list',
	'irc',
	'gnats',
	'nobody',
	'_apt',
	'Debian-exim'
]

def isInitialUser(username):
	if username in INITIAL_USERS:
		return True
	return False

#Done classical method to back related route (unit test this)
def hello():
	return 'Welcome to user hot program!'

#Done route method (test with deployment tests)
@app.route('/')
def route_hello(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':hello()})

#classical method to back related route
def getUsers():
	return getUserList()

#route method
@app.route('/getusers')
def route_getUsers(): #pragma: no cover
	#TODO
	data = {}
	data['code'] = '2000'
	data['msg'] = {}
	data['msg']['InitialUsers'] = INITIAL_USERS
	
	newUsers = []
	for user in getUsers():
		if(not isInitialUser(user)):
			newUsers.append(user)
	
	data['msg']['NewUsers'] = newUsers

	return jsonify(data)

#done classical method to back related route
def delUser(username):
	if username in getUserList():
		if isInitialUser(username):
			raise InitialUserException('cannot delete an initial user.')
		else:
			myoswrap.runCommandToRemoveUser(username)
			return 'deleted ' + username
	raise NonExistingUserException('The specified user (' + username + ') does not exist, cannot perform delete operation.')

#done route method
@app.route('/deluser')
def route_delUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = delUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except InitialUserException as iue:
			return make_response(jsonify({'code':'1003', 'msg':str(iue)}), 400)
		except NonExistingUserException as neue:
			return make_response(jsonify({'code':'1002', 'msg':str(neue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for deletion.'}), 400)

#classical method to back related route
def resetUsers():
	userDeletedCount = 0
	for user in getUserList():
		if not isInitialUser(user):
			delUser(user)
			userDeletedCount = userDeletedCount + 1 
	return userDeletedCount

#route method 
@app.route('/resetusers')
def route_resetUsers(): #pragma: no cover
	userDeletedCount = resetUsers()
	return jsonify({'code':'2000', 'msg':userDeletedCount})

#classical method to back related route
def addUser(username):
	if username not in getUserList():
		myoswrap.runCommandToAddUser(username)
		return 'added ' + username
	raise UserAlreadyExistsException('The specified user (' + username + ') already exists, cannot perform add operation.')

#route method 
@app.route('/adduser')
def route_addUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = addUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except UserAlreadyExistsException as uaee:
			return make_response(jsonify({'code':'1001', 'msg':str(uaee)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for adding.'}), 400)

#classical method to back related route
def getLogs():
	logs = []
	for line in myoswrap.readFileByLine('/var/log/auth.log'):
		logs.append(line)
	return logs

#route method 
@app.route('/getlog')
def route_getLogs(): #pragma: no cover
	logs = getLogs()
	return jsonify({'code':'2000', 'msg':logs})

if __name__ == "__main__": #pragma: no cover
	print("NOT NOORMAL")
	app.run(debug=True, host='0.0.0.0', port=5555)


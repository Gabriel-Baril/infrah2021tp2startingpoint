import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_isInitialUser_WhenInitial(self, mock_oswrap):
        actual = userAPI.isInitialUser('root')
        self.assertTrue(actual)

    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_isInitialUser_WhenNonInitial(self, mock_oswrap):
        actual = userAPI.isInitialUser('gab')
        self.assertFalse(actual)
    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getUserList_WhenOneUser(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/root:/bin/bash']
        actual = userAPI.getUserList()
        self.assertIn('root', actual)
        #check the three possible mock calls
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getUserList_WhenMultipleUsers(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/root:/bin/bash', 'daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin', 'Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
        actual = userAPI.getUserList()
        self.assertIn('root', actual)
        self.assertIn('daemon', actual)
        self.assertIn('Roy', actual)
        #check the three possible mock calls
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getLog_WhenNoLog(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = []
        actual = userAPI.getLogs()
        self.assertEqual(actual, [])
        #check the three possible mock calls
        mock_oswrap.readFileByLine.assert_called_with('/var/log/auth.log')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getLog_WhenLogs(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = ['Mar 13 21:45:56 d7574b45618e useradd[80]: new group: name=tawin, GID=1000\n', 'Mar 13 21:45:56 d7574b45618e useradd[80]: new user: name=tawin, UID=1000, GID=1000, home=/home/tawin, shell=/bin/sh\n']
        actual = userAPI.getLogs()
        self.assertIn('group', actual[0])
        self.assertIn('user', actual[1])
        #check the three possible mock calls
        mock_oswrap.readFileByLine.assert_called_with('/var/log/auth.log')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()
    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getUsers_WhenOnlyInitialUsers(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = userAPI.INITIAL_USERS
        actual = userAPI.getUsers()
        self.assertEqual(actual, userAPI.INITIAL_USERS)
        #check the three possible mock calls
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getUsers_WhenInitialUsersAndCustomUsers(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = userAPI.INITIAL_USERS + ['gab', 'tawin']
        actual = userAPI.getUsers()
        self.assertIn('root', actual)
        self.assertIn('gab', actual)
        self.assertIn('tawin', actual)
        #check the three possible mock calls
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

        
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_getUsers_WhenOnlyCustomUsers(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = ['gab', 'tawin']
        actual = userAPI.getUsers()
        
        self.assertNotIn('root', actual)
        self.assertIn('gab', actual)
        self.assertIn('tawin', actual)
        
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_resetUsers_WhenInitialUsers(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = userAPI.INITIAL_USERS
        actual = userAPI.resetUsers()
        
        EXPECTED_USER_DELETED_COUNT = 0
        self.assertEqual(actual, EXPECTED_USER_DELETED_COUNT)
        
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_not_called()
    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_resetUsers_whenOnlyInitialUser(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = ['gab', 'tawin']
        actual = userAPI.resetUsers()

        EXPECTED_USER_DELETED_COUNT = 2
        self.assertEqual(actual, EXPECTED_USER_DELETED_COUNT)

        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_called()

    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_resetUsers_WhenInitialUsersAndCustomUsers(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = userAPI.INITIAL_USERS + ['gab', 'tawin']
        actual = userAPI.resetUsers()
        
        EXPECTED_USER_DELETED_COUNT = 2
        self.assertEqual(actual, EXPECTED_USER_DELETED_COUNT)
        
        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_not_called()
        mock_oswrap.runCommandToRemoveUser.assert_called()

    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_addUser(self, mock_oswrap):
        mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
        userAPI.addUser('Gab')

        mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
        mock_oswrap.runCommandToAddUser.assert_called_with('Gab')
        mock_oswrap.runCommandToRemoveUser.assert_not_called()

    
    @unittest.mock.patch('codeAPI.userAPI.myoswrap')
    def test_addUser_whenUserAlreadyExists(self, mock_oswrap):
        with self.assertRaises(UserAlreadyExistsException) as context:
            mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
            userAPI.addUser('Roy')
            
            mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
            mock_oswrap.runCommandToAddUser.assert_not_called()
            mock_oswrap.runCommandToRemoveUser.assert_not_called()

if __name__ == '__main__':
    unittest.main()


import unittest
import requests
import json
from codeAPI import userAPI


IP = "127.0.0.1"
PORT = "5555"
URL = "http://" + IP + ":" + PORT + "/"

class BasicTests(unittest.TestCase):
    def test_addUser(self):
        response = requests.get(URL + "adduser?username=Roy")
        self.assertEqual(200, response.status_code)

        response = requests.get(URL + "getusers")
        obj = json.loads(response.content.decode('utf-8'))
        self.assertIn('Roy', obj['msg']['NewUsers'])

        response = requests.get(URL + "deluser?username=Roy")
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('2000', obj['code'])

        response = requests.get(URL + "getusers")
        obj = json.loads(response.content.decode('utf-8'))
        self.assertNotIn('Roy', obj['msg']['NewUsers'])

    def test_addUserNoParam(self):
        response = requests.get(URL + "adduser")
        self.assertEqual(400, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('1000', obj['code'])

    def test_addUserExisting(self):
        response = requests.get(URL + "adduser?username=root")
        self.assertEqual(400, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('1001', obj['code'])
    
    def test_getUsers_whenOnlyInitialUsers(self):
        response = requests.get(URL + "getusers")
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('2000', obj['code'])
        self.assertEqual(userAPI.INITIAL_USERS, obj['msg']['InitialUsers'])
        
    def test_getUsers_whenInitialUsersAndCustomUsers(self):
        response = requests.get(URL + "adduser?username=Roy")
        self.assertEqual(200, response.status_code)

        response = requests.get(URL + "getusers")
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('2000', obj['code'])
        self.assertEqual(userAPI.INITIAL_USERS, obj['msg']['InitialUsers'])
        self.assertEqual(['Roy'], obj['msg']['NewUsers'])

        response = requests.get(URL + "deluser?username=Roy")
        self.assertEqual(200, response.status_code)

    def test_resetusers_whenOnlyInitialUsers(self):
        response = requests.get(URL + "resetusers")
        self.assertEqual(200, response.status_code)
        
        EXPECTED_USER_DELETED_COUNT = 0
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('2000', obj['code'])
        self.assertEqual(obj['msg'], EXPECTED_USER_DELETED_COUNT)

    
    def test_resetusers_whenInitialUsersAndCustomUsers(self):
        response = requests.get(URL + "adduser?username=Roy")
        self.assertEqual(200, response.status_code)

        response = requests.get(URL + "resetusers")
        self.assertEqual(200, response.status_code)
        
        EXPECTED_USER_DELETED_COUNT = 1
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual('2000', obj['code'])
        self.assertEqual(obj['msg'], EXPECTED_USER_DELETED_COUNT)
        
        response = requests.get(URL + "deluser?username=Roy")
        self.assertEqual(400, response.status_code)
    
    def test_getlog_whenAddingUser(self):
        old_response = requests.get(URL + "getlog")
        self.assertEqual(200, old_response.status_code)
        
        user_response = requests.get(URL + "adduser?username=Roy")
        self.assertEqual(200, user_response.status_code)

        new_response = requests.get(URL + "getlog")
        self.assertEqual(200, old_response.status_code)

        old_obj = json.loads(old_response.content.decode('utf-8'))
        new_obj = json.loads(new_response.content.decode('utf-8'))
        self.assertTrue(len(new_obj['msg']) == len(old_obj['msg']) + 2)

        response = requests.get(URL + "deluser?username=Roy")
        self.assertEqual(200, response.status_code)

    
    def test_getlog_whenRemovingUser(self):
        old_response = requests.get(URL + "getlog")
        self.assertEqual(200, old_response.status_code)
        
        user_response = requests.get(URL + "adduser?username=Roy")
        self.assertEqual(200, user_response.status_code)
        
        response = requests.get(URL + "deluser?username=Roy")
        self.assertEqual(200, response.status_code)

        new_response = requests.get(URL + "getlog")
        self.assertEqual(200, old_response.status_code)

        old_obj = json.loads(old_response.content.decode('utf-8'))
        new_obj = json.loads(new_response.content.decode('utf-8'))
        self.assertTrue(len(new_obj['msg']) == len(old_obj['msg']) + 5)

if __name__ == '__main__':
    unittest.main()